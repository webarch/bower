An Ansible Galaxy role to install [Bower](https://bower.io/), this depends on `npm` and it does a HTTP HEAD request to `https://github.com/bower/bower/releases/latest` to see what path this URL redirect to in order to find what the latest version is and only installs a new version if necessary. 

Add this to your `requirements.yml` file:

```yml
- name: nodejs
  src: https://git.coop/webarch/nodejs.git
  version: master
  scm: git

- name: bower
  src: https://git.coop/webarch/bower.git
  versions: master
  scm: git
```
